# Packetmap
A tool to create a simple heatmap of source addresses in a pcap file.

Disclaimer; I wrote this for fun an a few hours. Please don't use it for anything too serious without doing plenty of testing for yourself.

## What's the point?
I had read https://idea.popcount.org/2016-09-20-strange-loop---ip-spoofing/ and saw the IP maps they used to determine if it's likely source addresses are spoofed. Essentially I thought to myself "That's neat!" and decided to make a quick, and very simplified version.

Under normal network traffic the heatmap is usually pretty sparesly populated, however a DDoS attack with spoofed source addresses will likely give a far more randomized distribution.

## How does it work?
Essentially it maps the first and second octet of the IPv4 source address to the X and Y coordinates of a 256x256 pixel image. It also generates a few images with some filters applied. (TCP, TCP-SYN, UDP, DNS & NTP.) This means each pixel corresponds to a /16 IP block.

The red value of the pixel is determined by the relative number of packets received with a source address in that netblock.

# Usage
```
ruby packetmap.rb networktrace.pcap
```
It will output the 6 .png files in the current directory.

## VLANs
The ruby-pcap gem isn't able to deal with tagged VLAN packets. If you're mapping a packet capture from an interface that uses tagged VLANs you will need to strip VLAN tags from the packet capture first. You can use tcprewrite from the tcpreplay toolsuite to accomplish this.
```
tcprewrite --enet-vlan=del  --infile=input.pcap --outfile=output.pcap
```

# Requirements
- Ruby 1.9.3 or newer
- ruby-pcap gem
- chunky_png gem

# Example output
## Normal traffic capture
![](images/normal.png)

## Hping3 udp flood with randomized source
![](images/hping3.png)

You can see the source randomization for hping3 is actually not that great. Ideally a random distribution should look like random noise.
