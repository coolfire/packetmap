# frozen_string_literal: true

require 'chunky_png'
require 'pcap'

# Initialize empty 256x256 arrays
tot = Array.new(256) { Array.new(256, 0) }
tcp = Array.new(256) { Array.new(256, 0) }
syn = Array.new(256) { Array.new(256, 0) }
udp = Array.new(256) { Array.new(256, 0) }
dns = Array.new(256) { Array.new(256, 0) }
ntp = Array.new(256) { Array.new(256, 0) }

# Read and loop over pcap file
pc = Pcap::Capture.open_offline(ARGV[0])

pc.each do |pkt|
  next unless pkt.ip?
  oct = pkt.ip_src.to_s.split('.')
  for i in 0..3 do
    oct[i] = oct[i].to_i
  end

  # Add one to total
  tot[oct[0]][oct[1]] += 1
  if pkt.tcp?
    # Add one to tcp
    tcp[oct[0]][oct[1]] += 1
    if pkt.tcp_syn?
      # Add one to syn
      syn[oct[0]][oct[1]] += 1
    end
  elsif pkt.udp?
    # Add one to udp
    udp[oct[0]][oct[1]] += 1
    if pkt.udp_dport == 53
      # Add one to dns
      dns[oct[0]][oct[1]] += 1
    elsif pkt.udp_dport == 123
      # Add one to ntp
      ntp[oct[0]][oct[1]] += 1
    end
  end
end

# Get max packet counts
tot_max = tot.max.max
tcp_max = tcp.max.max
syn_max = syn.max.max
udp_max = udp.max.max
dns_max = dns.max.max
ntp_max = ntp.max.max

# Calculate scaling factor for red value
tot_factor = tot_max == 0 ? 0 : 255.0 / tot_max
tcp_factor = tcp_max == 0 ? 0 : 255.0 / tcp_max
syn_factor = syn_max == 0 ? 0 : 255.0 / syn_max
udp_factor = udp_max == 0 ? 0 : 255.0 / udp_max
dns_factor = dns_max == 0 ? 0 : 255.0 / dns_max
ntp_factor = ntp_max == 0 ? 0 : 255.0 / ntp_max

# Initialize images
tot_png = ChunkyPNG::Image.new(256, 256, ChunkyPNG::Color::WHITE)
tcp_png = ChunkyPNG::Image.new(256, 256, ChunkyPNG::Color::WHITE)
syn_png = ChunkyPNG::Image.new(256, 256, ChunkyPNG::Color::WHITE)
udp_png = ChunkyPNG::Image.new(256, 256, ChunkyPNG::Color::WHITE)
dns_png = ChunkyPNG::Image.new(256, 256, ChunkyPNG::Color::WHITE)
ntp_png = ChunkyPNG::Image.new(256, 256, ChunkyPNG::Color::WHITE)

# Loop over arrays to set pixel values
for i in 0..255
  for j in 0..255
    tot_rval = (tot[i][j] * tot_factor).ceil.to_i
    tot_png[i, j] = ChunkyPNG::Color.rgba(tot_rval, 0, 0, 255) if tot_rval > 0
    tcp_rval = (tcp[i][j] * tcp_factor).ceil.to_i
    tcp_png[i, j] = ChunkyPNG::Color.rgba(tcp_rval, 0, 0, 255) if tcp_rval > 0
    syn_rval = (syn[i][j] * syn_factor).ceil.to_i
    syn_png[i, j] = ChunkyPNG::Color.rgba(syn_rval, 0, 0, 255) if syn_rval > 0
    udp_rval = (udp[i][j] * udp_factor).ceil.to_i
    udp_png[i, j] = ChunkyPNG::Color.rgba(udp_rval, 0, 0, 255) if udp_rval > 0
    dns_rval = (dns[i][j] * dns_factor).ceil.to_i
    dns_png[i, j] = ChunkyPNG::Color.rgba(dns_rval, 0, 0, 255) if dns_rval > 0
    ntp_rval = (ntp[i][j] * ntp_factor).ceil.to_i
    ntp_png[i, j] = ChunkyPNG::Color.rgba(ntp_rval, 0, 0, 255) if ntp_rval > 0
  end
end

# Write image files
tot_png.save('total.png', interlace: true)
tcp_png.save('tcp.png', interlace: true)
syn_png.save('syn.png', interlace: true)
udp_png.save('udp.png', interlace: true)
dns_png.save('dns.png', interlace: true)
ntp_png.save('ntp.png', interlace: true)
